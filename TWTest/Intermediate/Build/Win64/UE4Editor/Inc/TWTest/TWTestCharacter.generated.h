// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TWTEST_TWTestCharacter_generated_h
#error "TWTestCharacter.generated.h already included, missing '#pragma once' in TWTestCharacter.h"
#endif
#define TWTEST_TWTestCharacter_generated_h

#define TWTest_Source_TWTest_TWTestCharacter_h_14_RPC_WRAPPERS
#define TWTest_Source_TWTest_TWTestCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define TWTest_Source_TWTest_TWTestCharacter_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATWTestCharacter(); \
	friend struct Z_Construct_UClass_ATWTestCharacter_Statics; \
public: \
	DECLARE_CLASS(ATWTestCharacter, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TWTest"), NO_API) \
	DECLARE_SERIALIZER(ATWTestCharacter)


#define TWTest_Source_TWTest_TWTestCharacter_h_14_INCLASS \
private: \
	static void StaticRegisterNativesATWTestCharacter(); \
	friend struct Z_Construct_UClass_ATWTestCharacter_Statics; \
public: \
	DECLARE_CLASS(ATWTestCharacter, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TWTest"), NO_API) \
	DECLARE_SERIALIZER(ATWTestCharacter)


#define TWTest_Source_TWTest_TWTestCharacter_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATWTestCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATWTestCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATWTestCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATWTestCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATWTestCharacter(ATWTestCharacter&&); \
	NO_API ATWTestCharacter(const ATWTestCharacter&); \
public:


#define TWTest_Source_TWTest_TWTestCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATWTestCharacter(ATWTestCharacter&&); \
	NO_API ATWTestCharacter(const ATWTestCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATWTestCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATWTestCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATWTestCharacter)


#define TWTest_Source_TWTest_TWTestCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Mesh1P() { return STRUCT_OFFSET(ATWTestCharacter, Mesh1P); } \
	FORCEINLINE static uint32 __PPO__FP_Gun() { return STRUCT_OFFSET(ATWTestCharacter, FP_Gun); } \
	FORCEINLINE static uint32 __PPO__FP_MuzzleLocation() { return STRUCT_OFFSET(ATWTestCharacter, FP_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__VR_Gun() { return STRUCT_OFFSET(ATWTestCharacter, VR_Gun); } \
	FORCEINLINE static uint32 __PPO__VR_MuzzleLocation() { return STRUCT_OFFSET(ATWTestCharacter, VR_MuzzleLocation); } \
	FORCEINLINE static uint32 __PPO__FirstPersonCameraComponent() { return STRUCT_OFFSET(ATWTestCharacter, FirstPersonCameraComponent); } \
	FORCEINLINE static uint32 __PPO__R_MotionController() { return STRUCT_OFFSET(ATWTestCharacter, R_MotionController); } \
	FORCEINLINE static uint32 __PPO__L_MotionController() { return STRUCT_OFFSET(ATWTestCharacter, L_MotionController); }


#define TWTest_Source_TWTest_TWTestCharacter_h_11_PROLOG
#define TWTest_Source_TWTest_TWTestCharacter_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TWTest_Source_TWTest_TWTestCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	TWTest_Source_TWTest_TWTestCharacter_h_14_RPC_WRAPPERS \
	TWTest_Source_TWTest_TWTestCharacter_h_14_INCLASS \
	TWTest_Source_TWTest_TWTestCharacter_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TWTest_Source_TWTest_TWTestCharacter_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TWTest_Source_TWTest_TWTestCharacter_h_14_PRIVATE_PROPERTY_OFFSET \
	TWTest_Source_TWTest_TWTestCharacter_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	TWTest_Source_TWTest_TWTestCharacter_h_14_INCLASS_NO_PURE_DECLS \
	TWTest_Source_TWTest_TWTestCharacter_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TWTEST_API UClass* StaticClass<class ATWTestCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TWTest_Source_TWTest_TWTestCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
