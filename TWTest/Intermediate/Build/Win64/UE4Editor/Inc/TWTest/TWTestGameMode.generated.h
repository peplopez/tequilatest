// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TWTEST_TWTestGameMode_generated_h
#error "TWTestGameMode.generated.h already included, missing '#pragma once' in TWTestGameMode.h"
#endif
#define TWTEST_TWTestGameMode_generated_h

#define TWTest_Source_TWTest_TWTestGameMode_h_12_RPC_WRAPPERS
#define TWTest_Source_TWTest_TWTestGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define TWTest_Source_TWTest_TWTestGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATWTestGameMode(); \
	friend struct Z_Construct_UClass_ATWTestGameMode_Statics; \
public: \
	DECLARE_CLASS(ATWTestGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/TWTest"), TWTEST_API) \
	DECLARE_SERIALIZER(ATWTestGameMode)


#define TWTest_Source_TWTest_TWTestGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesATWTestGameMode(); \
	friend struct Z_Construct_UClass_ATWTestGameMode_Statics; \
public: \
	DECLARE_CLASS(ATWTestGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/TWTest"), TWTEST_API) \
	DECLARE_SERIALIZER(ATWTestGameMode)


#define TWTest_Source_TWTest_TWTestGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	TWTEST_API ATWTestGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATWTestGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(TWTEST_API, ATWTestGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATWTestGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	TWTEST_API ATWTestGameMode(ATWTestGameMode&&); \
	TWTEST_API ATWTestGameMode(const ATWTestGameMode&); \
public:


#define TWTest_Source_TWTest_TWTestGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	TWTEST_API ATWTestGameMode(ATWTestGameMode&&); \
	TWTEST_API ATWTestGameMode(const ATWTestGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(TWTEST_API, ATWTestGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATWTestGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATWTestGameMode)


#define TWTest_Source_TWTest_TWTestGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define TWTest_Source_TWTest_TWTestGameMode_h_9_PROLOG
#define TWTest_Source_TWTest_TWTestGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TWTest_Source_TWTest_TWTestGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	TWTest_Source_TWTest_TWTestGameMode_h_12_RPC_WRAPPERS \
	TWTest_Source_TWTest_TWTestGameMode_h_12_INCLASS \
	TWTest_Source_TWTest_TWTestGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TWTest_Source_TWTest_TWTestGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TWTest_Source_TWTest_TWTestGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	TWTest_Source_TWTest_TWTestGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	TWTest_Source_TWTest_TWTestGameMode_h_12_INCLASS_NO_PURE_DECLS \
	TWTest_Source_TWTest_TWTestGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TWTEST_API UClass* StaticClass<class ATWTestGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TWTest_Source_TWTest_TWTestGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
