// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FVector;
struct FHitResult;
#ifdef TWTEST_TWTestProjectile_generated_h
#error "TWTestProjectile.generated.h already included, missing '#pragma once' in TWTestProjectile.h"
#endif
#define TWTEST_TWTestProjectile_generated_h

#define TWTest_Source_TWTest_TWTestProjectile_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define TWTest_Source_TWTest_TWTestProjectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnHit) \
	{ \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_HitComp); \
		P_GET_OBJECT(AActor,Z_Param_OtherActor); \
		P_GET_OBJECT(UPrimitiveComponent,Z_Param_OtherComp); \
		P_GET_STRUCT(FVector,Z_Param_NormalImpulse); \
		P_GET_STRUCT_REF(FHitResult,Z_Param_Out_Hit); \
		P_FINISH; \
		P_NATIVE_BEGIN; \
		P_THIS->OnHit(Z_Param_HitComp,Z_Param_OtherActor,Z_Param_OtherComp,Z_Param_NormalImpulse,Z_Param_Out_Hit); \
		P_NATIVE_END; \
	}


#define TWTest_Source_TWTest_TWTestProjectile_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATWTestProjectile(); \
	friend struct Z_Construct_UClass_ATWTestProjectile_Statics; \
public: \
	DECLARE_CLASS(ATWTestProjectile, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TWTest"), NO_API) \
	DECLARE_SERIALIZER(ATWTestProjectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define TWTest_Source_TWTest_TWTestProjectile_h_12_INCLASS \
private: \
	static void StaticRegisterNativesATWTestProjectile(); \
	friend struct Z_Construct_UClass_ATWTestProjectile_Statics; \
public: \
	DECLARE_CLASS(ATWTestProjectile, AActor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TWTest"), NO_API) \
	DECLARE_SERIALIZER(ATWTestProjectile) \
	static const TCHAR* StaticConfigName() {return TEXT("Game");} \



#define TWTest_Source_TWTest_TWTestProjectile_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATWTestProjectile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATWTestProjectile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATWTestProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATWTestProjectile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATWTestProjectile(ATWTestProjectile&&); \
	NO_API ATWTestProjectile(const ATWTestProjectile&); \
public:


#define TWTest_Source_TWTest_TWTestProjectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATWTestProjectile(ATWTestProjectile&&); \
	NO_API ATWTestProjectile(const ATWTestProjectile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATWTestProjectile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATWTestProjectile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATWTestProjectile)


#define TWTest_Source_TWTest_TWTestProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CollisionComp() { return STRUCT_OFFSET(ATWTestProjectile, CollisionComp); } \
	FORCEINLINE static uint32 __PPO__ProjectileMovement() { return STRUCT_OFFSET(ATWTestProjectile, ProjectileMovement); }


#define TWTest_Source_TWTest_TWTestProjectile_h_9_PROLOG
#define TWTest_Source_TWTest_TWTestProjectile_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TWTest_Source_TWTest_TWTestProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	TWTest_Source_TWTest_TWTestProjectile_h_12_RPC_WRAPPERS \
	TWTest_Source_TWTest_TWTestProjectile_h_12_INCLASS \
	TWTest_Source_TWTest_TWTestProjectile_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TWTest_Source_TWTest_TWTestProjectile_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TWTest_Source_TWTest_TWTestProjectile_h_12_PRIVATE_PROPERTY_OFFSET \
	TWTest_Source_TWTest_TWTestProjectile_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	TWTest_Source_TWTest_TWTestProjectile_h_12_INCLASS_NO_PURE_DECLS \
	TWTest_Source_TWTest_TWTestProjectile_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TWTEST_API UClass* StaticClass<class ATWTestProjectile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TWTest_Source_TWTest_TWTestProjectile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
