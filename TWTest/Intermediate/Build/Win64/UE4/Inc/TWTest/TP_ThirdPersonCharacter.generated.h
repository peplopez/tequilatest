// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TWTEST_TP_ThirdPersonCharacter_generated_h
#error "TP_ThirdPersonCharacter.generated.h already included, missing '#pragma once' in TP_ThirdPersonCharacter.h"
#endif
#define TWTEST_TP_ThirdPersonCharacter_generated_h

#define TWTest_Source_TWTest_TP_ThirdPerson_TP_ThirdPersonCharacter_h_12_RPC_WRAPPERS
#define TWTest_Source_TWTest_TP_ThirdPerson_TP_ThirdPersonCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define TWTest_Source_TWTest_TP_ThirdPerson_TP_ThirdPersonCharacter_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATP_ThirdPersonCharacter(); \
	friend struct Z_Construct_UClass_ATP_ThirdPersonCharacter_Statics; \
public: \
	DECLARE_CLASS(ATP_ThirdPersonCharacter, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TWTest"), NO_API) \
	DECLARE_SERIALIZER(ATP_ThirdPersonCharacter)


#define TWTest_Source_TWTest_TP_ThirdPerson_TP_ThirdPersonCharacter_h_12_INCLASS \
private: \
	static void StaticRegisterNativesATP_ThirdPersonCharacter(); \
	friend struct Z_Construct_UClass_ATP_ThirdPersonCharacter_Statics; \
public: \
	DECLARE_CLASS(ATP_ThirdPersonCharacter, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TWTest"), NO_API) \
	DECLARE_SERIALIZER(ATP_ThirdPersonCharacter)


#define TWTest_Source_TWTest_TP_ThirdPerson_TP_ThirdPersonCharacter_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATP_ThirdPersonCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATP_ThirdPersonCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATP_ThirdPersonCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATP_ThirdPersonCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATP_ThirdPersonCharacter(ATP_ThirdPersonCharacter&&); \
	NO_API ATP_ThirdPersonCharacter(const ATP_ThirdPersonCharacter&); \
public:


#define TWTest_Source_TWTest_TP_ThirdPerson_TP_ThirdPersonCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATP_ThirdPersonCharacter(ATP_ThirdPersonCharacter&&); \
	NO_API ATP_ThirdPersonCharacter(const ATP_ThirdPersonCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATP_ThirdPersonCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATP_ThirdPersonCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATP_ThirdPersonCharacter)


#define TWTest_Source_TWTest_TP_ThirdPerson_TP_ThirdPersonCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(ATP_ThirdPersonCharacter, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__FollowCamera() { return STRUCT_OFFSET(ATP_ThirdPersonCharacter, FollowCamera); }


#define TWTest_Source_TWTest_TP_ThirdPerson_TP_ThirdPersonCharacter_h_9_PROLOG
#define TWTest_Source_TWTest_TP_ThirdPerson_TP_ThirdPersonCharacter_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TWTest_Source_TWTest_TP_ThirdPerson_TP_ThirdPersonCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	TWTest_Source_TWTest_TP_ThirdPerson_TP_ThirdPersonCharacter_h_12_RPC_WRAPPERS \
	TWTest_Source_TWTest_TP_ThirdPerson_TP_ThirdPersonCharacter_h_12_INCLASS \
	TWTest_Source_TWTest_TP_ThirdPerson_TP_ThirdPersonCharacter_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TWTest_Source_TWTest_TP_ThirdPerson_TP_ThirdPersonCharacter_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TWTest_Source_TWTest_TP_ThirdPerson_TP_ThirdPersonCharacter_h_12_PRIVATE_PROPERTY_OFFSET \
	TWTest_Source_TWTest_TP_ThirdPerson_TP_ThirdPersonCharacter_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	TWTest_Source_TWTest_TP_ThirdPerson_TP_ThirdPersonCharacter_h_12_INCLASS_NO_PURE_DECLS \
	TWTest_Source_TWTest_TP_ThirdPerson_TP_ThirdPersonCharacter_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TWTEST_API UClass* StaticClass<class ATP_ThirdPersonCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TWTest_Source_TWTest_TP_ThirdPerson_TP_ThirdPersonCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
