// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TWTEST_TWTNpc_generated_h
#error "TWTNpc.generated.h already included, missing '#pragma once' in TWTNpc.h"
#endif
#define TWTEST_TWTNpc_generated_h

#define TWTest_Source_TWTest_TWTNpc_h_11_RPC_WRAPPERS
#define TWTest_Source_TWTest_TWTNpc_h_11_RPC_WRAPPERS_NO_PURE_DECLS
#define TWTest_Source_TWTest_TWTNpc_h_11_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATWTNpc(); \
	friend struct Z_Construct_UClass_ATWTNpc_Statics; \
public: \
	DECLARE_CLASS(ATWTNpc, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TWTest"), NO_API) \
	DECLARE_SERIALIZER(ATWTNpc)


#define TWTest_Source_TWTest_TWTNpc_h_11_INCLASS \
private: \
	static void StaticRegisterNativesATWTNpc(); \
	friend struct Z_Construct_UClass_ATWTNpc_Statics; \
public: \
	DECLARE_CLASS(ATWTNpc, ACharacter, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TWTest"), NO_API) \
	DECLARE_SERIALIZER(ATWTNpc)


#define TWTest_Source_TWTest_TWTNpc_h_11_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATWTNpc(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATWTNpc) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATWTNpc); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATWTNpc); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATWTNpc(ATWTNpc&&); \
	NO_API ATWTNpc(const ATWTNpc&); \
public:


#define TWTest_Source_TWTest_TWTNpc_h_11_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATWTNpc(ATWTNpc&&); \
	NO_API ATWTNpc(const ATWTNpc&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATWTNpc); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATWTNpc); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATWTNpc)


#define TWTest_Source_TWTest_TWTNpc_h_11_PRIVATE_PROPERTY_OFFSET
#define TWTest_Source_TWTest_TWTNpc_h_9_PROLOG
#define TWTest_Source_TWTest_TWTNpc_h_11_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TWTest_Source_TWTest_TWTNpc_h_11_PRIVATE_PROPERTY_OFFSET \
	TWTest_Source_TWTest_TWTNpc_h_11_RPC_WRAPPERS \
	TWTest_Source_TWTest_TWTNpc_h_11_INCLASS \
	TWTest_Source_TWTest_TWTNpc_h_11_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TWTest_Source_TWTest_TWTNpc_h_11_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TWTest_Source_TWTest_TWTNpc_h_11_PRIVATE_PROPERTY_OFFSET \
	TWTest_Source_TWTest_TWTNpc_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
	TWTest_Source_TWTest_TWTNpc_h_11_INCLASS_NO_PURE_DECLS \
	TWTest_Source_TWTest_TWTNpc_h_11_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TWTEST_API UClass* StaticClass<class ATWTNpc>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TWTest_Source_TWTest_TWTNpc_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
