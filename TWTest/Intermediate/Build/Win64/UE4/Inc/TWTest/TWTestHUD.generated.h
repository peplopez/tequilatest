// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TWTEST_TWTestHUD_generated_h
#error "TWTestHUD.generated.h already included, missing '#pragma once' in TWTestHUD.h"
#endif
#define TWTEST_TWTestHUD_generated_h

#define TWTest_Source_TWTest_TWTestHUD_h_12_RPC_WRAPPERS
#define TWTest_Source_TWTest_TWTestHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define TWTest_Source_TWTest_TWTestHUD_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATWTestHUD(); \
	friend struct Z_Construct_UClass_ATWTestHUD_Statics; \
public: \
	DECLARE_CLASS(ATWTestHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/TWTest"), NO_API) \
	DECLARE_SERIALIZER(ATWTestHUD)


#define TWTest_Source_TWTest_TWTestHUD_h_12_INCLASS \
private: \
	static void StaticRegisterNativesATWTestHUD(); \
	friend struct Z_Construct_UClass_ATWTestHUD_Statics; \
public: \
	DECLARE_CLASS(ATWTestHUD, AHUD, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/TWTest"), NO_API) \
	DECLARE_SERIALIZER(ATWTestHUD)


#define TWTest_Source_TWTest_TWTestHUD_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATWTestHUD(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATWTestHUD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATWTestHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATWTestHUD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATWTestHUD(ATWTestHUD&&); \
	NO_API ATWTestHUD(const ATWTestHUD&); \
public:


#define TWTest_Source_TWTest_TWTestHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATWTestHUD(ATWTestHUD&&); \
	NO_API ATWTestHUD(const ATWTestHUD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATWTestHUD); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATWTestHUD); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATWTestHUD)


#define TWTest_Source_TWTest_TWTestHUD_h_12_PRIVATE_PROPERTY_OFFSET
#define TWTest_Source_TWTest_TWTestHUD_h_9_PROLOG
#define TWTest_Source_TWTest_TWTestHUD_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TWTest_Source_TWTest_TWTestHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	TWTest_Source_TWTest_TWTestHUD_h_12_RPC_WRAPPERS \
	TWTest_Source_TWTest_TWTestHUD_h_12_INCLASS \
	TWTest_Source_TWTest_TWTestHUD_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TWTest_Source_TWTest_TWTestHUD_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TWTest_Source_TWTest_TWTestHUD_h_12_PRIVATE_PROPERTY_OFFSET \
	TWTest_Source_TWTest_TWTestHUD_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	TWTest_Source_TWTest_TWTestHUD_h_12_INCLASS_NO_PURE_DECLS \
	TWTest_Source_TWTest_TWTestHUD_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TWTEST_API UClass* StaticClass<class ATWTestHUD>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TWTest_Source_TWTest_TWTestHUD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
