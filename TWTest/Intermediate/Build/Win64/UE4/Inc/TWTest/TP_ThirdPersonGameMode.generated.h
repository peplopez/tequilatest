// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TWTEST_TP_ThirdPersonGameMode_generated_h
#error "TP_ThirdPersonGameMode.generated.h already included, missing '#pragma once' in TP_ThirdPersonGameMode.h"
#endif
#define TWTEST_TP_ThirdPersonGameMode_generated_h

#define TWTest_Source_TWTest_TP_ThirdPerson_TP_ThirdPersonGameMode_h_12_RPC_WRAPPERS
#define TWTest_Source_TWTest_TP_ThirdPerson_TP_ThirdPersonGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define TWTest_Source_TWTest_TP_ThirdPerson_TP_ThirdPersonGameMode_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATP_ThirdPersonGameMode(); \
	friend struct Z_Construct_UClass_ATP_ThirdPersonGameMode_Statics; \
public: \
	DECLARE_CLASS(ATP_ThirdPersonGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/TWTest"), TWTEST_API) \
	DECLARE_SERIALIZER(ATP_ThirdPersonGameMode)


#define TWTest_Source_TWTest_TP_ThirdPerson_TP_ThirdPersonGameMode_h_12_INCLASS \
private: \
	static void StaticRegisterNativesATP_ThirdPersonGameMode(); \
	friend struct Z_Construct_UClass_ATP_ThirdPersonGameMode_Statics; \
public: \
	DECLARE_CLASS(ATP_ThirdPersonGameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/TWTest"), TWTEST_API) \
	DECLARE_SERIALIZER(ATP_ThirdPersonGameMode)


#define TWTest_Source_TWTest_TP_ThirdPerson_TP_ThirdPersonGameMode_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	TWTEST_API ATP_ThirdPersonGameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATP_ThirdPersonGameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(TWTEST_API, ATP_ThirdPersonGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATP_ThirdPersonGameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	TWTEST_API ATP_ThirdPersonGameMode(ATP_ThirdPersonGameMode&&); \
	TWTEST_API ATP_ThirdPersonGameMode(const ATP_ThirdPersonGameMode&); \
public:


#define TWTest_Source_TWTest_TP_ThirdPerson_TP_ThirdPersonGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	TWTEST_API ATP_ThirdPersonGameMode(ATP_ThirdPersonGameMode&&); \
	TWTEST_API ATP_ThirdPersonGameMode(const ATP_ThirdPersonGameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(TWTEST_API, ATP_ThirdPersonGameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATP_ThirdPersonGameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATP_ThirdPersonGameMode)


#define TWTest_Source_TWTest_TP_ThirdPerson_TP_ThirdPersonGameMode_h_12_PRIVATE_PROPERTY_OFFSET
#define TWTest_Source_TWTest_TP_ThirdPerson_TP_ThirdPersonGameMode_h_9_PROLOG
#define TWTest_Source_TWTest_TP_ThirdPerson_TP_ThirdPersonGameMode_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TWTest_Source_TWTest_TP_ThirdPerson_TP_ThirdPersonGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	TWTest_Source_TWTest_TP_ThirdPerson_TP_ThirdPersonGameMode_h_12_RPC_WRAPPERS \
	TWTest_Source_TWTest_TP_ThirdPerson_TP_ThirdPersonGameMode_h_12_INCLASS \
	TWTest_Source_TWTest_TP_ThirdPerson_TP_ThirdPersonGameMode_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TWTest_Source_TWTest_TP_ThirdPerson_TP_ThirdPersonGameMode_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TWTest_Source_TWTest_TP_ThirdPerson_TP_ThirdPersonGameMode_h_12_PRIVATE_PROPERTY_OFFSET \
	TWTest_Source_TWTest_TP_ThirdPerson_TP_ThirdPersonGameMode_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	TWTest_Source_TWTest_TP_ThirdPerson_TP_ThirdPersonGameMode_h_12_INCLASS_NO_PURE_DECLS \
	TWTest_Source_TWTest_TP_ThirdPerson_TP_ThirdPersonGameMode_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TWTEST_API UClass* StaticClass<class ATP_ThirdPersonGameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TWTest_Source_TWTest_TP_ThirdPerson_TP_ThirdPersonGameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
