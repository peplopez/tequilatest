// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "TWTestGameMode.h"
#include "TWTestHUD.h"
#include "TWTestCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATWTestGameMode::ATWTestGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonBP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ATWTestHUD::StaticClass();
}
